import java.nio.file.Files
import java.nio.file.Path
import kotlin.math.max
import kotlin.system.measureTimeMillis
import kotlin.time.measureTime

enum class Reco(val lettre : Char) {
    X('X'),
    Y('Y'),
    Z('Z')
}

fun main(args: Array<String>) {

    fun part1(input: List<String>): Int {
        var totalScore = 0

        for (jeu in input) {
            totalScore += when (jeu) {
                "A X", "B Y", "C Z" -> 3
                "A Z", "B X", "C Y" -> 0
                "A Y", "B Z", "C X" -> 6
                else -> 0
            } + (jeu.last()-'X' + 1)
        }

        return totalScore
    }

    fun part2(input: List<String>): Int {
        var totalScore = 0
        val parties = mapOf("A A" to 3, "B B" to 3, "C C" to 3,
            "A C" to 0, "B A" to 0, "C B" to 0,
            "A B" to 6, "B C" to 6, "C A" to 6)

        for (jeu in input) {
            //println(jeu)
            /*
            val jeuFinal = jeu.slice(0..1) + when (jeu.last()) {
                            'X' -> "ABCA".run { get(indexOfLast { it==jeu.first() }-1) }
                            'Y' -> jeu.get(0)
                            'Z' -> "ABCA".run { get(indexOf(jeu.first())+1) }
                            else -> throw Exception("Oups calcul jeuFinal")
                        }
             */
            //V2 en passant par une Enum ce qui rend le code plus idiomatique
            val jeuFinal = jeu.slice(0..1) + when (Reco.valueOf(jeu.last().toString())) {
                        Reco.X -> "ABCA".run { get(indexOfLast { it==jeu.first() }-1) }
                        Reco.Y -> jeu[0]
                        Reco.Z -> "ABCA".run { get(indexOf(jeu.first())+1) }
                    }

            //println(jeuFinal)
            // A > C > B > A
            totalScore += parties[jeuFinal]!! + (jeuFinal.last()-'A' + 1)
        }

        return totalScore
    }

    fun part2_2(input: List<String>): Int {
        val parties = mapOf("A A" to 3, "B B" to 3, "C C" to 3,
            "A C" to 0, "B A" to 0, "C B" to 0,
            "A B" to 6, "B C" to 6, "C A" to 6)

        return input.map { jeu ->
            jeu.slice(0..1) + when (Reco.valueOf(jeu.last().toString())) {
                Reco.X -> "ABCA".run { get(indexOfLast { it == jeu.first() } - 1) }
                Reco.Y -> jeu[0]
                Reco.Z -> "ABCA".run { get(indexOf(jeu.first()) + 1) }
            }
        }.map {
            parties[it]!! + (it.last()-'A' + 1)
        }.sum()

    }

    //val input = readInput("day02_test")
    //check(part1(input) == 15)

    val input = readInput("day02")
    //println(part1(input))
    println(part2_2(input))

    //val input = readInput("day02_2_test")
    //println(part2(input))


    //val t2 = measureTimeMillis { println(part2(input)) }
    //val t2v2 = measureTimeMillis { println(part2v2(input)) }
    //println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}