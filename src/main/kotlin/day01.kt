import java.nio.file.Files
import java.nio.file.Path
import kotlin.math.max
import kotlin.system.measureTimeMillis
import kotlin.time.measureTime


fun main(args: Array<String>) {
    println("Hello World!")
    fun part1(input: List<String>): Int {
        var maxCalories =0
        var nbCalories = 0

        for (calories in input ) {
            if (calories=="") {
                maxCalories = max(maxCalories,nbCalories)
                nbCalories=0
            } else {
                nbCalories += calories.toInt()
            }
        }
        return max(maxCalories,nbCalories)
    }

    fun part2v2(input: List<String>): Int {
        val lstCalories = mutableListOf<Int>()
        var nbCalories = 0

        for (calories in input ) {
            if (calories=="") {
                lstCalories += nbCalories
                nbCalories = 0
            } else {
                nbCalories += calories.toInt()
            }
        }
        if (nbCalories>0) lstCalories += nbCalories

        return lstCalories.run {
          sortDescending()
          take(3).sum()
        }

    }

    fun part2(input: List<String>): Int {
        val lstCalories = mutableListOf<Int>(0,0,0)
        var nbCalories = 0

        for (calories in input ) {
            if (calories=="") {
                if (nbCalories > lstCalories[0]) {
                    lstCalories[0] = nbCalories
                    lstCalories.sort()
                }
                nbCalories = 0;
            } else {
                nbCalories += calories.toInt()
            }
        }
        if (nbCalories > lstCalories[0]) lstCalories[0] = nbCalories

        return lstCalories.sum()

    }

    //Files.list(Path.of("./src/main/resources")).forEach { print(it.fileName) }

    // test if implementation meets criteria from the description, like:
    //val testInput = readInput("day01_test")
    //check(part1(testInput) == 1)
    val input = readInput("day01")
    println(part1(input))

    val t2 = measureTimeMillis { println(part2(input)) }
    val t2v2 = measureTimeMillis { println(part2v2(input)) }

    println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}