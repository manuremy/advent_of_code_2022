fun main(args: Array<String>) {

    val fnProperty : (Char) -> Int = { ch: Char -> if (ch.isLowerCase())  ch -'a' +1 else ch - 'A' + 27 }
    //fun Char.fnPropExt(): Int = if ( isLowerCase())  this -'a' +1 else this - 'A' + 27

    fun part1(input: List<String>): Int {
        return input.map { item ->
            val c1=item.substring(0,item.length/2)
            val c2=item.substring(item.length/2)
            fnProperty(c1.find { c2.indexOf(it)>=0 }!!)
        }.sum()
    }

    fun part2(input: List<String>): Int {
        return input.chunked(3) { group ->
            group[0].asSequence()
                .filter { group[1].indexOf(it)>=0 }
                .filter { group[2].indexOf(it)>=0 }
                //.map { it.fnPropExt() }
                .map{ fnProperty(it) }
                .first()
        }.sum()
    }


    //val input = readInput("day02_test")
    //check(part1(input) == 15)

    val input = readInput("day03")
    println(part1(input))
    //println(part22(input))

    //val input = readInput("day02_2_test")
    println(part2(input))


    //val t2 = measureTimeMillis { println(part2(input)) }
    //val t2v2 = measureTimeMillis { println(part2v2(input)) }
    //println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}