import kotlinx.coroutines.*

fun main() = runBlocking {
        val job = launch {
            repeat(1000) { i ->
                println("job: I'm sleeping $i ...")
                delay(500L)
            }
        }
        val job2 = launch(job) {
                println("job2 avant")
                repeat(3) {
                    println("job2 repeat avant")
                    launch {
                        println("inside avant")
                        delay(20000L);
                        println("inside après")
                    }
                    delay(2000L)
                    println("job2 repeat après")
                }
                println("job2 après")
            }.invokeOnCompletion {
                println("Job 2 complété")
            }

    delay(2300L) // delay a bit
    println("main: annulation job 1")
    job.cancel() // cancels the job
    job.join() // waits for job's completion
    println("main: Now I can quit.")
}