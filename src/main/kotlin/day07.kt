import kotlin.system.measureTimeMillis

open class Node (val parent:Node?, val name:String, val type: TypeNode, private val size: Int =0) {
    enum class TypeNode { FILE, DIRECTORY}

    var children = mutableMapOf<String, Node>()

    fun getTotalNodeSize() : Int {
        return size + children.values.sumOf { it.getTotalNodeSize() }
    }

    fun getAllDirectory() : List<Node> {
        val d1 = children.filter { (_, node) -> node.type==TypeNode.DIRECTORY }.values
        return d1 + d1.map { it.getAllDirectory() }.flatten()
    }

    public operator fun get(child:String) = children[child]!!
}

class Fs : Node(null, "/", Node.TypeNode.DIRECTORY) {
    companion object {
        const val TOTAL_SIZE = 70000000
    }
    private var currentNode = this as Node

    val fsSize: Int
        get() = getTotalNodeSize()

    fun touch(filename:String, size:Int) =  create(filename,Node.TypeNode.FILE,size)
    fun mkdir(name:String) = create(name,Node.TypeNode.DIRECTORY,0)
    private fun create(name:String, type:Node.TypeNode, size:Int) =
        //on ajoute l'enfant uniquement s'il n'existe pas déjà
        currentNode.children.putIfAbsent(name, Node(currentNode,name, type, size))

    fun cd(directory: String) {
        currentNode = when (directory) {
            ".." -> currentNode.parent?:this as Node
            "/" ->  this
            else ->
                currentNode.children[directory]!!
        }
    }

}
fun main(args: Array<String>) {
    fun createFS( input: List<String>) = Fs().also {fs ->
        input.forEach {
            it.split(" ").apply {
                when (this[0]) {
                    "$" -> if (this[1]=="cd") fs.cd(this[2])
                    "dir" -> fs.mkdir(this[1])
                    else -> fs.touch(filename=this[1], size=this[0].toInt())
                }
            }
        }
    }

    fun part1(input: List<String>): Int {
/*
        var fs = createFS(input);
        println("a: ${fs["a"].getTotalNodeSize()}")
        println("e: ${fs["a"]["e"].getTotalNodeSize()}")
        println("d: ${fs["d"].getTotalNodeSize()}")
        println("fs size: ${fs.fsSize}")
*/
        return createFS(input).getAllDirectory()
            .filter { it.getTotalNodeSize() <= 100000 }
            .sumOf { it.getTotalNodeSize() }
    }

    fun part2(input: List<String>): Int {
        val fs = createFS(input);
        val unusedNeedSpace = 30000000
        val minSpaceToDelete = unusedNeedSpace - (Fs.TOTAL_SIZE - fs.fsSize)
        println("Espace à libérer: $minSpaceToDelete")

        return createFS(input).getAllDirectory()
            .filter { it.getTotalNodeSize() >= minSpaceToDelete }
            .minOf { it.getTotalNodeSize() }

    }


    val input = readInput("day07")
    val t1 = measureTimeMillis { println(part1(input)) }
    val t2 = measureTimeMillis { println(part2(input)) }
    println ("t1: $t2 ms - t2 : $t2 ms")
}