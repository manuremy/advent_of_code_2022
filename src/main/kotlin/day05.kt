//On initie une liste immuable de 9 listes mutables
val stacks= List(9) { mutableListOf<Char>() }

class DerivedDestructured(private val d: MatchResult.Destructured)   {
    public operator fun component1(): Int = d.component1().toInt()
    public operator fun component2(): Int = d.component2().toInt()
    public operator fun component3(): Int = d.component3().toInt()
}


fun fn(input: List<String>, reverse:Boolean): String {
    for(i in 7 downTo 0) {
        //[B] [T] [M] [B] [J] [C] [T] [G] [N]
        input[i].chunked(4) { it[1]}
            .forEachIndexed { index, c -> c.takeIf { it != ' ' }?.let { stacks[index].add(it) }}
    }
    input.forEach {
        val re = kotlin.text.Regex("move (\\d+) from (\\d+) to (\\d+)")
        if (re.matches(it)) {
            //on recupère move, to, from en Int putôt qu'en String en effectuant une conversion
            val (move, from, to) = DerivedDestructured(re.find(it)!!.destructured)
            stacks[from - 1].run {
                stacks[to - 1] += takeLast(move).run { if (reverse) asReversed() else this }
                val fromResize = take(size - move)
                clear()
                addAll(fromResize)
            }
        }
    }
/*  ou dans le cas où move, from sont

    input.forEach {
        val re = kotlin.text.Regex("move (\\d+) from (\\d+) to (\\d+)")
        if (re.matches(it)) {
            val (move, from, to) = re.find(it)!!.destructured
            stacks[from.toInt() - 1].run {
                stacks[to.toInt() - 1] += takeLast(move.toInt()).run { if (reverse) asReversed() else this }
                val fromResize = take(size - move.toInt())
                clear()
                addAll(fromResize)
            }
        }
    }
 */
    return stacks.map { it.last() }
        .joinToString(separator = "")
}

fun main(args: Array<String>) {
    fun part1(input: List<String>): String {
        return fn(input,reverse = true)
    }

    fun part2(input: List<String>): String {
        return fn(input,reverse = false)
    }


    //val input = readInput("day04_test")
    val input = readInput("day05")
    println(part1(input))
    println(part2(input))


    //val t2 = measureTimeMillis { println(part2(input)) }
    //val t2v2 = measureTimeMillis { println(part2v2(input)) }
    //println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}