fun IntRange.contains(r: IntRange): Boolean {
    return this.contains(r.first) && this.contains(r.last)
}

fun IntRange.overlaps(r: IntRange): Boolean {
    //F1..L1 F2..L2
    return if (this.first < r.first)
        this.last >= r.first
    else
        r.last >= this.first
}

fun IntRange.Companion.from(s:String): IntRange =
    s.split("-").run {
        this[0].toInt()..this[1].toInt()
    }

val Boolean?.One_Zero
        get() = if (this!=null && this==true) 1 else 0


fun main(args: Array<String>) {
    fun part1(input: List<String>): Int {
        return input
            .map { it.split(",") }
            .map {
                val r1= IntRange.from(it[0])
                val r2= IntRange.from(it[1])
                (r1.contains(r2) || r2.contains(r1)).One_Zero
            }
            .sum()
    }

    fun part2(input: List<String>): Int {
        return input
            .map { it.split(",") }
            .map {
                val r1= IntRange.from(it[0])
                val r2= IntRange.from(it[1])
                r1.overlaps(r2).One_Zero
            }
            .sum()
    }


    //val input = readInput("day04_test")
    val input = readInput("day04")
    println(part1(input))
    println(part2(input))


    //val t2 = measureTimeMillis { println(part2(input)) }
    //val t2v2 = measureTimeMillis { println(part2v2(input)) }
    //println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}