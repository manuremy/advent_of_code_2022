import kotlin.system.measureTimeMillis

//pour la partie 2: on extrait la fonction
fun getWindowNum(signal: String, windowSize:Int) : Int =
    signal.windowed(windowSize, 1)
        .indexOfFirst { window ->
            window.groupingBy { it }
                .eachCount()
                .values
                .none { it >= 2 }
        }

fun main(args: Array<String>) {
    fun part1(input: List<String>): Int {
        val signal = input[0]

        val numWindow=
            //on crée une fenêtre de 4 caractères qu'on décale à chaque fois d'1 caractère
            signal.windowed(4,1)
                //on cherche la première fenetre qui ne comprend pas 2 fois le même caractère
            .indexOfFirst { window ->
                //on regroupe par caractère de la fenetre
                window.groupingBy { it }
                    //on compte le nombre d'occurences de chaque caractère
                    .eachCount()
                    //... et on cherche parmi les valeurs la première >= 2
                    .values
                    .none { it>=2 }}
        //la position recherchée est le numéro du dernier caractère de fenètre
        return numWindow + 4
    }

    fun part2(input: List<String>): Int {
        val windowSize=14
        return getWindowNum(input[0],windowSize) + windowSize
    }


    val input = readInput("day06")
    //println(part1(input))
    //println(part2(input))

    val t1 = measureTimeMillis { println(part2(input)) }
    val t2 = measureTimeMillis { println(part2(input)) }

    println ("t1: $t2 ms - t2 : $t2 ms")


}