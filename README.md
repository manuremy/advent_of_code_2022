AdventOfCode 2022



===============================

Jour 2

A,X: Rock=1 , B,Y: Paper=2 , C,Z: Scissors=3,
A > C > B > A
Victoire: 6, Nul: 3, défaite:0

AX, BY, CZ: 3
AZ, BX, CY: 0
AY, BZ, CX: 6

En fait: X: défaite, Y: match nul, Z: victoire

==================================
Jour 4

2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8

Combien de sections comprennent l'autre ?

